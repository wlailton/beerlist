package br.com.wlailton.core

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.wlailton.beerList.MainActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Start home activity
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        // Splash screen standby time
        Thread.sleep(1_500)
        // close splash activity
        finish()
    }
}
