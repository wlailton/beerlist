package br.com.wlailton.beerList

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import br.com.wlailton.DB.DAOs.BeerDao
import br.com.wlailton.DB.Database.AppDatabase
import br.com.wlailton.DB.Model.Beer
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



class BeerAdapter (private var context: Context,
                   private var dataSource: List<Beer>,
                   var beerListFav: List<Beer>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val beer: Beer = getItem(position) as Beer
        val rowView: View = inflater.inflate(R.layout.list_item_beer, parent, false)
        (rowView.findViewById(R.id.beer_list_text_id) as TextView).setText(beer.id.toString())
        (rowView.findViewById(R.id.beer_list_title) as TextView).setText(beer.name)
        (rowView.findViewById(R.id.beer_list_subtitle) as TextView).setText(beer.tagline)
        val detailButton: Button = rowView.findViewById(R.id.beer_fav_button) as Button
        val thumbnailImageView: ImageView = rowView.findViewById(R.id.beer_list_thumbnail) as ImageView
        Picasso.get().load(beer.image_url).placeholder(R.drawable.placeholder).into(thumbnailImageView)

        var fidBeer: List<Beer> = beerListFav.filter { it.id.equals(beer.id.toLong()) }
        if (fidBeer.size > 0) {
            detailButton.setBackgroundResource(R.drawable.staron)
        } else {
            detailButton.setBackgroundResource(R.drawable.staroff)
        }

        detailButton.setOnClickListener {
            var beer: Beer = dataSource.filter { it.id.equals(beer.id) }.first()
            getObservavleUpdateFavListBeer(beer).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { it:List<Beer> ->

                        beerListFav = it

                        if(beerListFav.filter { it.id.equals(beer.id.toLong()) }.size > 0) {
                            detailButton.setBackgroundResource(R.drawable.staron)
                        } else {
                            detailButton.setBackgroundResource(R.drawable.staroff)
                        }

                    }

        }
        return rowView
    }

    fun getObservavleUpdateFavListBeer(beer: Beer): Observable<List<Beer>> {
        var db: AppDatabase = AppDatabase.getInstance(context = context)!!
        var beerDao: BeerDao = db?.beerDao()
        return Observable.create { subscriber ->

            var id : Long? = db?.beerDao()?.getBeer(beer.id.toLong())?.id
            if(id != null) {
                db?.beerDao()?.delete(Beer(beer.id.toLong(),
                        beer.name,beer.description,
                        beer.tagline,
                        beer.image_url))
            } else {
                db?.beerDao()?.insert(
                        Beer(beer.id.toLong(),
                                beer.name,beer.description,
                                beer.tagline,
                                beer.image_url))
            }
            val list: List<Beer> = beerDao?.all()!!
            subscriber.onNext(
                    list?.sortedBy { it.name }
            )
            subscriber.onComplete()
        }
    }

    fun clear() {
        dataSource = listOf()
        beerListFav = listOf()
        notifyDataSetChanged()
    }

    fun addAll(listBeer: List<Beer>, listFavBeer: List<Beer>) {
        dataSource = listBeer
        beerListFav = listFavBeer
        notifyDataSetChanged()
    }

}

