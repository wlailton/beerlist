package br.com.wlailton.beerList

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import android.widget.TextView
import br.com.wlailton.DB.Model.Beer
import br.com.wlailton.beerDetail.BeerDetailActivity
import br.com.wlailton.beerListFav.BeerListFavActivity
import br.com.wlailton.services.RetrofitInitializer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity() {

    private lateinit var listView: ListView
    private var beerListQuery: List<Beer> = listOf()
    private var beerList: List<Beer> = listOf()
    private var beerListFav: List<Beer> = listOf()
    private var swipeContainer: SwipeRefreshLayout? = null

    // Activity life cycle method
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        listView = findViewById<ListView>(R.id.beer_list_view)
        listView.emptyView = findViewById<TextView>(R.id.text_view_loading_list_beer)
        swipeContainer = findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setOnRefreshListener(object: SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                getListBeers()
            }
        })

        setItemsListenersListView()
        getListBeers()
    }

    override fun onResume() {
        if(listView.adapter != null) {
            beerListFav = (listView.adapter as BeerAdapter).beerListFav
        }
        super.onResume()
        subscribeFavoritListBeer()
    }

    override fun onCreateOptionsMenu(menu: Menu) : Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchView: SearchView = menu.findItem(R.id.searchBeer).actionView as SearchView
        val searchManager: SearchManager =  getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String) : Boolean {
                configureBeerList(
                    beerList
                        .filter { it.name.contains(newText, ignoreCase = true) }
                        .sortedBy { it.name }
                )
                beerListQuery = beerList
                        .filter { it.name.contains(newText, ignoreCase = true) }
                        .sortedBy { it.name }
                return false
            }

        })

        searchView.setOnSearchClickListener(
            object : View.OnClickListener {
                override fun onClick (v: View?) {
                    findViewById<View>(R.id.text_view_loading_list_beer).visibility = View.GONE
                    listView.emptyView = findViewById<View>(R.id.layout_not_foud)
                    menu.findItem(R.id.menu_item_favbeer).setVisible(false);
                }
        })

        searchView.setOnCloseListener(object : SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                findViewById<View>(R.id.layout_not_foud).visibility = View.GONE
                menu.findItem(R.id.menu_item_favbeer).setVisible(true);
                menu.findItem(R.id.searchBeer).setVisible(true);
                configureBeerList(beerList.sortedBy { it.name })
                beerListQuery = listOf()
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_item_favbeer -> {
                this.onPause()
                val intent = Intent(this, BeerListFavActivity::class.java)
                startActivity(intent)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setItemsListenersListView() {
        val context: MainActivity = this
        listView.setOnItemClickListener { _, _, position, _ ->
            val selectedBeer: Beer
            if(beerListQuery.size > 0) {
                selectedBeer = beerListQuery[position]
            } else {
                selectedBeer = beerList[position]
            }
            val detailIntent: Intent = BeerDetailActivity.newIntent(context, selectedBeer)
            startActivity(detailIntent)
        }
    }

    private fun getListBeers() {
        val call: Call<List<Beer>> = RetrofitInitializer().beerService().list()
        call.enqueue(object : Callback<List<Beer>?> {
            override fun onResponse(call: Call<List<Beer>?>?, response: Response<List<Beer>?>?) {
                response?.body()?.let { it: List<Beer> ->
                    beerList = it.sortedBy { it.name }
                    (listView.adapter as BeerAdapter).clear()
                    configureBeerList(beerList)

                    swipeContainer?.isRefreshing = false
                }
            }

            override fun onFailure(call: Call<List<Beer>?>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }
        })
    }

    private fun configureBeerList() {
        listView.adapter = BeerAdapter(this, beerList, beerListFav)
    }

    private fun configureBeerList(beerList: List<Beer>) {
        listView.adapter = BeerAdapter(this, beerList, beerListFav)
    }

    private fun subscribeFavoritListBeer() {
        getObservavleListBeer().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it:List<Beer> ->
                    if(!it.size.equals(beerListFav.size)) {
                        beerListFav = it
                        configureBeerList()
                    }
                })
    }

}
