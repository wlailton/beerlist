package br.com.wlailton.beerList

import android.annotation.SuppressLint
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ListView
import br.com.wlailton.DB.DAOs.BeerDao
import br.com.wlailton.DB.Database.AppDatabase
import br.com.wlailton.DB.Model.Beer
import br.com.wlailton.broadcasts.ConnectivityReceiver
import io.reactivex.Observable

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {
    private val connectivityReceiver = ConnectivityReceiver();

    // Activity life cycle method
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver(connectivityReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    // Activity life cycle method
    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    // Activity life cycle method
    override fun onPause() {
        super.onPause()
//        unregisterReceiver(connectivityReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(connectivityReceiver)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            findViewById<ListView>(R.id.beer_list_view).visibility = View.GONE
            findViewById<View>(R.id.layout_off_line).visibility = View.VISIBLE
        } else {
            findViewById<ListView>(R.id.beer_list_view).visibility = View.VISIBLE
            findViewById<View>(R.id.layout_off_line).visibility = View.GONE
        }
    }

    fun getObservavleListBeer(): Observable<List<Beer>> {
        var db: AppDatabase = AppDatabase.getInstance(context = this)!!
        var beerDao: BeerDao = db?.beerDao()
        return Observable.create { subscriber ->
            val list: List<Beer> = beerDao?.all()!!
            subscriber.onNext(list?.sortedBy { it.name })
            subscriber.onComplete()
        }
    }

}