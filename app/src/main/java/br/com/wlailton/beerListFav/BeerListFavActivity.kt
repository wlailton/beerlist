package br.com.wlailton.beerListFav

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.widget.ListView
import br.com.wlailton.DB.DAOs.BeerDao
import br.com.wlailton.DB.Database.AppDatabase
import br.com.wlailton.DB.Model.Beer
import br.com.wlailton.beerDetail.BeerDetailActivity
import br.com.wlailton.beerList.BeerAdapter
import br.com.wlailton.beerList.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_beer_list_fav.*

class BeerListFavActivity : AppCompatActivity() {
    private lateinit var listView: ListView
    private var beerList: List<Beer> = listOf()
    private var swipeContainer: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_list_fav)
        setSupportActionBar(toolbar)
        listView = findViewById(R.id.beer_fav_list_view)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);

        swipeContainer = findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setOnRefreshListener(object: SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                updateBeerListFav()
            }
        })

        setItemsListenersListView()
        subscribeFavoritListBeer()
    }

    private fun updateBeerListFav() {
        // Aqui chama o serviço para atualizar as informações da lista de favoritos
        swipeContainer?.isRefreshing = false
    }

    private fun subscribeFavoritListBeer() {
        getObservavleListBeer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it: List<Beer> ->
                    listView.adapter = BeerAdapter(this, it, it)
                    beerList = it.sortedBy { it.name }
                })
    }

    fun getObservavleListBeer(): Observable<List<Beer>> {
        var db: AppDatabase = AppDatabase.getInstance(context = this)!!
        var beerDao: BeerDao = db?.beerDao()
        return Observable.create { subscriber ->
            val list: List<Beer> = beerDao?.all()!!
            subscriber.onNext(list?.sortedBy { it.name })
            subscriber.onComplete()
        }
    }

    private fun setItemsListenersListView() {
        val context: BeerListFavActivity = this
        listView.setOnItemClickListener { _, _, position, _ ->
            val selectedBeer: Beer = beerList[position]
            val detailIntent: Intent = BeerDetailActivity.newIntent(context, selectedBeer)
            startActivity(detailIntent)
        }
    }
}
