package br.com.wlailton.DB

import android.app.Application
import android.arch.persistence.room.Room
import br.com.wlailton.DB.Database.AppDatabase

class AppDatabaseSingleton: Application() {

    companion object {
        var database: AppDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        AppDatabaseSingleton.database = Room.databaseBuilder(this, AppDatabase::class.java,
                "list-beer-db").build()
    }
}
