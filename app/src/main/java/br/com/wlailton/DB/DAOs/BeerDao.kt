package br.com.wlailton.DB.DAOs

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import br.com.wlailton.DB.Model.Beer

@Dao
interface BeerDao {
    @Query("SELECT * FROM beer")
    fun all(): List<Beer>

    @Query("SELECT * FROM beer WHERE id LIKE :id")
    fun getBeer(id: Long): Beer

    @Insert()
    fun insert(weatherData: Beer)

    @Delete
    fun delete(weatherData: Beer)

}