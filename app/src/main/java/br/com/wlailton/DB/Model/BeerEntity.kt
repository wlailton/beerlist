package br.com.wlailton.DB.Model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
class Beer(@PrimaryKey
            @NonNull
           val id: Long = 0,
           val name: String,
           val description: String,
           val tagline: String,
           val image_url: String): Serializable