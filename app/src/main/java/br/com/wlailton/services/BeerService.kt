package br.com.wlailton.services

import br.com.wlailton.DB.Model.Beer
import retrofit2.Call
import retrofit2.http.GET

interface BeerService {
    @GET("beers")
    fun list(): Call<List<Beer>>
}