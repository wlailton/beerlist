package br.com.wlailton.beerDetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import br.com.wlailton.DB.Model.Beer
import br.com.wlailton.beerList.R
import com.squareup.picasso.Picasso

class BeerDetailActivity : AppCompatActivity() {

    companion object {
        const val BEER = "beer"

        fun newIntent(context: Context, beer: Beer): Intent {
            val detailIntent = Intent(context, BeerDetailActivity::class.java)
            detailIntent.putExtra(BEER, beer)
            return detailIntent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_detail)

        val beer: Beer = intent.extras.getSerializable(BEER) as Beer
        val thumbnailImageView = findViewById<ImageView>(R.id.beer_detail_thumbnail)
        Picasso.get().load(beer.image_url).placeholder(R.drawable.placeholder).into(thumbnailImageView)
        findViewById<TextView>(R.id.beer_detail_title).setText(beer.name)
        findViewById<TextView>(R.id.beer_detail_subtitle).setText(beer.tagline)
        findViewById<TextView>(R.id.beer_detail_description).setText(beer.description)

        val actionbar = supportActionBar
        actionbar!!.title = getResources().getString(R.string.title_activity_beer_list_detail)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}
